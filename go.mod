module gitee.com/tdxmkf123/gorm-driver-dameng

go 1.20

require (
	github.com/golang/snappy v0.0.4
	golang.org/x/text v0.14.0
	gorm.io/gorm v1.25.1
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
)
